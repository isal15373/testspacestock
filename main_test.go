package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Validate(t *testing.T) {
	person := Person{
		Name: "",
	}

	expected := errors.New("Name cannot be empty")
	err := person.Validate()
	assert.Equal(t, err, expected)

	person = Person{
		Name: "Jon Snow",
		Gender: "",
	}

	expected = errors.New("Gender is either Male or Female")
	err = person.Validate()
	assert.Equal(t, err, expected)

	person = Person{
		Name: "Jon Snow",
		Gender: "Male",
		Age: -1,
	}

	expected = errors.New("There is no such thing as negative age")
	err = person.Validate()
	assert.Equal(t, err, expected)

	person = Person{
		Name: "Jon Snow",
		Gender: "Male",
		Age: 23,
	}

	expected = nil
	err = person.Validate()
	assert.Equal(t, err, expected)
}