package main

import (
	"errors"
	"fmt"
	"strconv"
)

func sum(list []int) (total int) {
	index := 0
	totalList := len(list) - 1

	repeat:
	total = total + list[index]
	if index < totalList {
		index++
		goto repeat
	}

	return total
}

func contain(list []string, alphabet string) (result string) {

	index := 0
	totalList := len(list) - 1

repeat:

	if alphabet == list[index] {
		return "alphabet: " + alphabet + " is exist"
	}
	if index < totalList {
		index++
		goto repeat
	}

	return ""
}

func person() string {
	type person struct {
		name   string
		gender string
		age    int
	}

	res := person{
		name: "Jon Snow",
		gender: "Male",
		age: 24,
	}

	return res.name + ", "+ res.gender + ", " + strconv.Itoa(res.age)

}

type Person struct {
	Name   string
	Gender string
	Age    int
}

func (p Person) Validate() error {
	if p.Name == "" {
		return errors.New("Name cannot be empty")
	}

	if p.Gender != "Male" && p.Gender != "Female" {
		return errors.New("Gender is either Male or Female")
	}

	if p.Age < 0 {
		return errors.New("There is no such thing as negative age")
	}
	return nil
}

type Invoice struct {
	To string
	Amount  int
}

func groupInvoiceByPerson(invoices []Invoice) (newInvoice []Invoice) {

	temp := make(map[string]int)
	for k, v := range invoices {
		if amount, ok := temp[v.To]; ok {
			if invoices[k].To ==  v.To {
				temp[v.To] = invoices[k].Amount + amount
			}
		} else {
			temp[v.To] = invoices[k].Amount
		}
	}

	for key, val := range temp {
		invoice := Invoice{
			To:key,
			Amount:val,
		}
		newInvoice = append(newInvoice, invoice)
	}
	return newInvoice
}

func main(){
	// sum
	list := []int{2, 3, 4}
	fmt.Println(sum(list))

	//contain
	listAlphabet := []string{"a", "b", "c"}
	fmt.Println(contain(listAlphabet, "c"))

	// person
	fmt.Println(person())

	// invoice
	invoices := []Invoice{
		{To: "John Doe", Amount: 120000},
		{To: "John Doe", Amount: 80000},
		{To: "Jane Doe", Amount: 100000},
	}
	fmt.Printf("groupInvoiceByPerson: %+v:", groupInvoiceByPerson(invoices))
}
